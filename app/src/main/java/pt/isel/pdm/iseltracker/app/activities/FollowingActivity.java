package pt.isel.pdm.iseltracker.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Switch;

import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import pt.isel.pdm.iseltracker.app.R;
import pt.isel.pdm.iseltracker.app.adapters.ParseFollowingAdapter;
import pt.isel.pdm.iseltracker.app.models.ParseFollowers;

/**
 * Created by ISEL on 26/06/2014.
 */
public class FollowingActivity extends Activity {

    private static final String TAG = FollowingActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);

        ParseFollowingAdapter adapter = new ParseFollowingAdapter(
                this,
                new ParseQueryAdapter.QueryFactory<ParseFollowers>() {
                    @Override
                    public ParseQuery<ParseFollowers> create() {
                        ParseQuery<ParseFollowers> query = new ParseQuery<ParseFollowers>(ParseFollowers.class);
                        query.whereEqualTo(ParseFollowers.STATE, ParseFollowers.STATE_PENDING);
                        query.whereEqualTo(ParseFollowers.FOLLOWING, ParseUser.getCurrentUser());
                        return query;
                    }
                },
                true,
                true);

        ListView lv = (ListView)findViewById(R.id.follow_list);
        lv.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu()");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_done:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
