package pt.isel.pdm.iseltracker.app.models;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

/**
 * Created by André Jonas on 02/06/2014.
 */
@ParseClassName("Label")
public class ParseLabel extends ParseObject {

    private static final String BSSID = "bssid";
    private static final String LOCATION = "location";
    private static final String DESCRIPTOR = "descriptor";

    public ParseLabel(){}

    public String getBSSID(){ return getString(BSSID); }
    public ParseGeoPoint getLocation(){ return getParseGeoPoint(LOCATION); }
    public String getDescriptor(){ return getString(DESCRIPTOR); }

}
