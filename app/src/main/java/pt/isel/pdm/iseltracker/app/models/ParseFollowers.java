package pt.isel.pdm.iseltracker.app.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by RPA on 26/06/2014.
 */

@ParseClassName("Followers")
public class ParseFollowers extends ParseObject {

    public static final String FOLLOWER = "follower";
    public static final String FOLLOWING = "following";
    public static final String STATE = "state";

    public static final String STATE_PENDING = "pending";
    public static final String STATE_REVOKED = "revoked";
    public static final String STATE_GRANTED = "granted";

    /**
     * Este construtor é necessário à API do Parse, não deve ser utilizado para construir objetos
     * deste tipo
     */
    public ParseFollowers(){}

    public ParseFollowers(ParseUser follower, ParseUser following){
        put(FOLLOWER, follower);
        put(FOLLOWING, following);
        put(STATE, STATE_PENDING);
    }

    public String getState(){
        return getString(STATE);
    }

    public ParseUser getFollower(){
        return getParseUser(FOLLOWER);
    }

    public ParseUser getFollowing(){
        return getParseUser(FOLLOWING);
    }

    public void setState(String newState){
        String currState = getString(STATE);
        if( !currState.equals(STATE_REVOKED)){ // se currState for REVOKED não faz nada
            if(newState.equals(STATE_GRANTED) && currState.equals(STATE_PENDING)){put(STATE, STATE_GRANTED);}
            if(newState.equals(STATE_REVOKED)){put(STATE, STATE_REVOKED);}
        }
    }
}
