package pt.isel.pdm.iseltracker.app.models;

import android.location.Location;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by André Jonas on 29/05/2014.
 */
@ParseClassName("Post")
public class ParsePost extends ParseObject {

    public static final String BSSID = "bssid";
    public static final String LOCATION = "location";
    public static final String LABEL = "label";
    public static final String USER = "user";

    /**
     * Este construtor é necessário à API do Parse, não deve ser utilizado para construir objetos
     * deste tipo
     */
    public ParsePost(){}

    public ParsePost(String bssid){
        put(BSSID, bssid);
        put(USER, ParseUser.getCurrentUser());
        //put(LABEL, TODO getLabel());
        //put(USER, TODO);
    }

    public ParsePost(Location location){
        put(LOCATION, getGeoPoint(location));
        put(USER, ParseUser.getCurrentUser());
        //put(LABEL, TODO getLabel());
        //put(USER, TODO);
    }

    public ParsePost(ParseGeoPoint location){
        put(LOCATION, location);
        put(USER, ParseUser.getCurrentUser());
        //put(LABEL, TODO getLabel());
        //put(USER, TODO);
    }

    private static ParseGeoPoint getGeoPoint(Location location){
        return new ParseGeoPoint(location.getLatitude(), location.getLongitude());
    }

    public static ParseQuery<ParsePost> query() {
        return ParseQuery.getQuery(ParsePost.class);
    }

    public String getBSSID(){ return getString(BSSID); }
    //public void setBSSID(String bssid){ put(BSSID, bssid); }
    public ParseGeoPoint getLocation(){ return getParseGeoPoint(LOCATION); }
    public ParseUser getUser() throws ParseException{ return getParseUser(USER); }
    public ParseLabel getLabel() throws ParseException{ return (ParseLabel)getParseObject(LABEL); }
}
