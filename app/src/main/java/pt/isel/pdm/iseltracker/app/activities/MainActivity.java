package pt.isel.pdm.iseltracker.app.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;

import com.parse.CountCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import pt.isel.pdm.iseltracker.app.LocationNegotiator;
import pt.isel.pdm.iseltracker.app.R;
import pt.isel.pdm.iseltracker.app.services.RefreshService;
import pt.isel.pdm.iseltracker.app.models.ParseFollowers;
import pt.isel.pdm.iseltracker.app.models.ParsePost;


public class MainActivity extends Activity implements CompoundButton.OnCheckedChangeListener{

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String PREFS_TRACKING_ENABLED_BOOLEAN = "TRACKING_ENABLED";
    private LocationNegotiator locNegotiator;

    private ParseQueryAdapter<ParsePost> adapter;

    public static ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CheckPendingRequests();
        /*
        adapter = new ParsePostAdapter(this,
                new ParseQueryAdapter.QueryFactory<ParsePost>() {
                    @Override
                    public ParseQuery<ParsePost> create() {
                        // Query específica pelos 10 primeiros Post's, por ordem decrescente,
                        // pela data de criação.
                        ParseQuery<ParsePost> query = new ParseQuery<ParsePost>(ParsePost.class);
                        query.addDescendingOrder("createdAt");
                        query.setLimit(10);
                        return query;
                    }
                },
                R.layout.list_item_default
        );
        // Sem paginação
        adapter.setPaginationEnabled(false);
        // Callback para idenfiticar se foram carregados algumas posições, com Toast para o user.
        adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<ParsePost>() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(List<ParsePost> parsePosts, Exception e) {
                if(parsePosts != null && parsePosts.size() == 0){
                    Toast.makeText(getApplicationContext(), R.string.no_posts, Toast.LENGTH_SHORT)
                        .show();
                }
            }
        });
        */
        listView = (ListView) this.findViewById(R.id.listView);
        //View.setAdapter(adapter);

        // Chama explícitamente o serviço de refrescamento.
        startService(new Intent(this, RefreshService.class));

        locNegotiator = LocationNegotiator.getInstance(this);

    }

    private void CheckPendingRequests(){
        ParseQuery<ParseFollowers> query = ParseQuery.getQuery(ParseFollowers.class);
        query.whereEqualTo(ParseFollowers.STATE, ParseFollowers.STATE_PENDING);
        query.whereEqualTo(ParseFollowers.FOLLOWING, ParseUser.getCurrentUser());
        query.countInBackground(new CountCallback() {
            public void done(int count, ParseException e) {
                if (e == null) {
                    if(count > 0){
                        Intent i = new Intent(MainActivity.this, FollowingActivity.class);
                        startActivity(i);
                    }
                } else {
                    // Request Failed
                }
            }
        });
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart()");
        super.onStart();
        Intent intent = new Intent(this, RefreshService.class);
        PendingIntent pi = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setInexactRepeating(
                AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() * 5 * 60 * 1000,
                5 * 60 * 1000,
                pi);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();
        //locNegotiator.negotiate();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop()");
        super.onStop();
        Intent intent = new Intent(this, RefreshService.class);
        PendingIntent pi = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pi);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu()");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        // Referências e inicializações do Switch na ActioBar
        MenuItem item = menu.findItem(R.id.action_target);
        Switch tracking = (Switch) item.getActionView();
        tracking.setChecked(locNegotiator.isTracking());
        tracking.setOnCheckedChangeListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_target:
                return true;
            case R.id.action_logout:
                ParseUser.logOut();
                // Inicia explícitamente a Activity de signup e termina a Activity atual
                Intent intent = new Intent(this, SignupActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_revoke:
                startActivity(new Intent(this, RevokeActivity.class));
                return true;
            case R.id.action_pending:
                startActivity(new Intent(this, FollowingActivity.class));
                return true;
            case R.id.action_invite:
                startActivity(new Intent(this, InviteActivity.class));
                return true;
            case R.id.action_refresh:
                startService(new Intent(this, RefreshService.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d(TAG, "onCheckedChanged()");
        locNegotiator.setTracking(isChecked);
        locNegotiator.negotiate();
    }
}
