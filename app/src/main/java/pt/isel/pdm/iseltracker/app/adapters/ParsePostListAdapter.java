package pt.isel.pdm.iseltracker.app.adapters;

import android.app.Service;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.List;

import pt.isel.pdm.iseltracker.app.R;
import pt.isel.pdm.iseltracker.app.models.ParseLabel;
import pt.isel.pdm.iseltracker.app.models.ParsePost;

/**
 * Created by André Jonas on 30/06/2014.
 */
public class ParsePostListAdapter extends ArrayAdapter<ParsePost> {


    private final static int itemResId = R.layout.list_item_default;
    private final List<ParsePost> objects;
    private LayoutInflater inflater;

    public ParsePostListAdapter(Context context, List<ParsePost> objects) {
        super(context, itemResId, objects);
        this.objects = objects;
        inflater = (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public View getView(int position, View v, ViewGroup parent) {
        if(v == null){
            v = inflater.inflate(itemResId, null);
        }

        final ParsePost object = objects.get(position);

        final TextView location = (TextView) v.findViewById(R.id.main);
        final TextView user = (TextView) v.findViewById(R.id.secondary);

        // Async value assignment
        try {
            object.getLabel().fetchInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if (e == null) {
                        location.setText(R.string.unknown);
                    }
                    if(parseObject != null) {
                        ParseLabel l = (ParseLabel) parseObject;
                        location.setText(l.getDescriptor());
                    }else{
                        location.setText(R.string.unknown);
                    }
                }
            });
        }catch (Exception e){
            location.setText(R.string.unknown);
        }

        try {
            object.getUser().fetchInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if (e == null) {
                        user.setText(R.string.unknown);
                    }
                    if(parseObject != null){
                        ParseUser u = (ParseUser) parseObject;
                        user.setText(u.getUsername());
                    }else{
                        location.setText(R.string.unknown);
                    }
                }
            });
        } catch (Exception e) {
            user.setText(R.string.unknown);
        }
        return v;
    }
}
