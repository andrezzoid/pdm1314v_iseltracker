package pt.isel.pdm.iseltracker.app.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import pt.isel.pdm.iseltracker.app.R;
import pt.isel.pdm.iseltracker.app.models.ParseFollowers;

/**
 * Created by André Jonas on 30/06/2014.
 */
public class ParseUserInviteAdapter extends ParseQueryAdapter<ParseUser> {

    private static final int itemViewId = R.layout.list_item_follow;

    public ParseUserInviteAdapter(Context context,
            ParseQueryAdapter.QueryFactory<ParseUser> queryFactory) {
        super(context, queryFactory, itemViewId);
    }

    public ParseUserInviteAdapter(Context context,
            Class<ParseUser> clazz) {
        super(context, clazz, itemViewId);
    }

    @Override
    public View getItemView(final ParseUser object, View v, ViewGroup parent) {
        //return super.getItemView(object, v, parent);

        if (v == null) {
            v = View.inflate(getContext(), itemViewId, null);
        }

        final TextView username = (TextView) v.findViewById(R.id.username);
        ((Button) v.findViewById(R.id.grant)).setVisibility(View.GONE);
        final Button invite = (Button) v.findViewById(R.id.revoke);

        username.setText(object.getUsername());
        invite.setText(R.string.invite);

        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invite.setClickable(false);
                invite.setEnabled(false);
                ParseFollowers newFollowing = new ParseFollowers(ParseUser.getCurrentUser(), object);
                newFollowing.saveInBackground();
            }
        });

        return v;
    }
}
