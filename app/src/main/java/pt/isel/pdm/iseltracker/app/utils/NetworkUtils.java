package pt.isel.pdm.iseltracker.app.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by André Jonas on 26-03-2014.
 */
public class NetworkUtils {

    /**
     * Verifica se existe conectividade, qualquer que seja (e.g. Wi-Fi, 3G, etc.)
     * @param ctx
     * @return
     */
    public static boolean isConnected(Context ctx) {
        ConnectivityManager connMgr = (ConnectivityManager)
                ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null &&
                networkInfo.isConnected();
    }

    /**
     * Verifica se existe conectividade por Wi-Fi.
     * @param ctx
     * @return
     */
    public static boolean isConnectedWiFi(Context ctx) {
        ConnectivityManager connMgr = (ConnectivityManager)
                ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null &&
                networkInfo.isConnected() &&
                networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
    }
}
