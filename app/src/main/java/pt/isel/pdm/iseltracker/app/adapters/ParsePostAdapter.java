package pt.isel.pdm.iseltracker.app.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import pt.isel.pdm.iseltracker.app.R;
import pt.isel.pdm.iseltracker.app.models.ParseLabel;
import pt.isel.pdm.iseltracker.app.models.ParsePost;

/**
 * Created by André Jonas on 02/06/2014.
 */
public class ParsePostAdapter extends ParseQueryAdapter<ParsePost> {

    private int itemViewId;

    public ParsePostAdapter(Context context, QueryFactory<ParsePost> queryFactory, int itemViewResource) {
        super(context, queryFactory, itemViewResource);
        itemViewId = itemViewResource;
    }

    public ParsePostAdapter(Context context, Class<? extends ParseObject> clazz, int itemViewResource) {
        super(context, clazz, itemViewResource);
        itemViewId = itemViewResource;
    }

    @Override
    public View getItemView(ParsePost object, View v, ViewGroup parent) {
        //return super.getItemView(object, v, parent);

        if (v == null) {
            v = View.inflate(getContext(), itemViewId, null);
        }

        final TextView location = (TextView) v.findViewById(R.id.main);
        final TextView user = (TextView) v.findViewById(R.id.secondary);

        // Async value assignment
        try {
            object.getLabel().fetchInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if (e == null) {
                        location.setText(R.string.unknown);
                    }
                    if(parseObject != null) {
                        ParseLabel l = (ParseLabel) parseObject;
                        location.setText(l.getDescriptor());
                    }else{
                        location.setText(R.string.unknown);
                    }
                }
            });
        }catch (Exception e){
            location.setText(R.string.unknown);
        }

        try {
            object.getUser().fetchInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if (e == null) {
                        user.setText(R.string.unknown);
                    }
                    if(parseObject != null){
                        ParseUser u = (ParseUser) parseObject;
                        user.setText(u.getUsername());
                    }else{
                        location.setText(R.string.unknown);
                    }
                }
            });
        } catch (Exception e) {
            user.setText(R.string.unknown);
        }
        return v;
    }
}
