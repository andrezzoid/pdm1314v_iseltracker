package pt.isel.pdm.iseltracker.app.adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import pt.isel.pdm.iseltracker.app.R;
import pt.isel.pdm.iseltracker.app.models.ParseFollowers;
import pt.isel.pdm.iseltracker.app.models.ParseLabel;
import pt.isel.pdm.iseltracker.app.models.ParsePost;

/**
 * Created by André Jonas on 26/06/2014.
 */
public class ParseFollowingAdapter extends ParseQueryAdapter<ParseFollowers> {

    private static final int itemViewId = R.layout.list_item_follow;
    private static final String TAG = ParseFollowingAdapter.class.getSimpleName();
    private final boolean hasRevoke;
    private final boolean hasGrant;

    public ParseFollowingAdapter(Context context,
                                 QueryFactory<ParseFollowers> queryFactory,
                                 boolean hasGrant,
                                 boolean hasRevoke) {
        super(context, queryFactory, itemViewId);
        this.hasGrant = hasGrant;
        this.hasRevoke = hasRevoke;
    }

    public ParseFollowingAdapter(Context context,
                                 Class<ParseFollowers> clazz,
                                 boolean hasGrant,
                                 boolean hasRevoke) {
        super(context, clazz, itemViewId);
        this.hasGrant = hasGrant;
        this.hasRevoke = hasRevoke;
    }

    @Override
    public View getItemView(final ParseFollowers object, View v, ViewGroup parent) {
        Log.d(TAG, "getItemView");
        //return super.getItemView(object, v, parent);

        if (v == null) {
            v = View.inflate(getContext(), itemViewId, null);
        }

        final TextView username = (TextView) v.findViewById(R.id.username);
        final Button grant = (Button) v.findViewById(R.id.grant);
        final Button revoke = (Button) v.findViewById(R.id.revoke);

        try {
            object.getFollower().fetchInBackground(new GetCallback<ParseUser>() {
                @Override
                public void done(ParseUser parseObject, ParseException e) {
                    ParseUser follower = (ParseUser) parseObject;
                    username.setText(follower.getUsername());
                }
            });
        }catch (Exception e){
            username.setText(R.string.unknown);
            grant.setClickable(false);
            grant.setEnabled(false);
            revoke.setClickable(false);
            revoke.setEnabled(false);
        }

        // Mostra ou esconde os botões, conforme especificado no construtor
        // Adiciona eventos aos botões
        if(!hasGrant) {
            grant.setVisibility(View.GONE);
        }else{
            grant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    object.setState(ParseFollowers.STATE_GRANTED);
                    object.saveInBackground();
                    grant.setClickable(false);
                    grant.setEnabled(false);
                    revoke.setClickable(false);
                    revoke.setEnabled(false);
                }
            });
        }

        if(!hasRevoke) {
            revoke.setVisibility(View.GONE);
        }else{
            revoke.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    object.setState(ParseFollowers.STATE_REVOKED);
                    object.saveInBackground();
                    grant.setClickable(false);
                    grant.setEnabled(false);
                    revoke.setClickable(false);
                    revoke.setEnabled(false);
                }
            });
        }
        return v;
    }
}
