package pt.isel.pdm.iseltracker.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.BaseAdapter;

import com.parse.ParseException;
import com.parse.SaveCallback;

import pt.isel.pdm.iseltracker.app.models.ParsePost;
import pt.isel.pdm.iseltracker.app.receivers.MobileReceiver;
import pt.isel.pdm.iseltracker.app.receivers.WiFiReceiver;

/**
 * Created by André Jonas on 30/05/2014.
 */
public class LocationNegotiator {
    private static final String TAG = LocationNegotiator.class.getSimpleName();
    private static final String PREFS_LOCATION_NEGOTIATOR = "PREFS_LOCATION_NEGOTIATOR";
    private static final String PREFS_TRACKING_ENABLED_BOOLEAN = "TRACKING_ENABLED";

    public static final int LOCATION_MINIMUM_TIME = 5* 1000;           //30 * 60 * 1000;    // Meia hora
    public static final int LOCATION_MINIMUM_DISTANCE = 50;            // 50 metros

    private Context context;
    private LocationManager locationManager;
    private SharedPreferences prefs;
    private boolean isTracking;

    private LocationListener locationListener;

    public LocationNegotiator(Context context){
        this.context = context;
        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        this.locationListener = new GPSLocationListener(context);
        // O estado da localização é armazenado nas SharedPreferences
        prefs = context.getSharedPreferences(
                PREFS_LOCATION_NEGOTIATOR,
                Context.MODE_PRIVATE);
        isTracking = prefs.getBoolean(PREFS_TRACKING_ENABLED_BOOLEAN, false);
        setTracking(isTracking);
    }

    //
    // Singleton. Apenas deverá existir uma instância desta classe.
    private static LocationNegotiator instance;

    public static LocationNegotiator getInstance (Context context){
        if(instance == null){
            instance = new LocationNegotiator(context);
        }
        return instance;
    }

    public LocationListener getLocationListener() {
        return locationListener;
    }

    public boolean isTracking(){ return isTracking; }

    public void setTracking(boolean isTracking){
        this.isTracking = isTracking;

        //
        // Armazena o estado da localização nas SharedPreferences
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREFS_TRACKING_ENABLED_BOOLEAN, isTracking);
        editor.apply();

        //
        // Activar/Desactivar captura de localização
        ComponentName wifiReceiver = new ComponentName(context, WiFiReceiver.class);
        ComponentName mobileReceiver = new ComponentName(context, MobileReceiver.class);

        PackageManager pm = context.getPackageManager();
        if(isTracking){
            Log.d(TAG, "Tracking is ON");
            pm.setComponentEnabledSetting(
                    wifiReceiver,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
            pm.setComponentEnabledSetting(
                    mobileReceiver,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
        }else{
            Log.d(TAG, "Tracking is OFF");
            locationManager.removeUpdates(locationListener);
            pm.setComponentEnabledSetting(
                    wifiReceiver,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
            pm.setComponentEnabledSetting(
                    mobileReceiver,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);

        }
    }

    public void negotiate() {
        if(!isTracking) {
            // Not tracking, do nothing.
            return;
        }
        Log.d(TAG, "Negotiating the Location Service.");

        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        // PROTOCOLO DE NEGOCIAÇÃO:
        // ------------------------

        // 1. A negociação de localização começa com tentativa de obter posicionamento por Wi-Fi. É
        // enviado um broadcast com um intent explícito para WiFiReceiver.class com a conexão activa
        // como extra, pois num caso normal de mudança de estado do Wi-Fi, o sistema Android faz o
        // mesmo e este Receiver deverá ser genérico o suficiente para responder a tudo.
        Intent wifiIntent = new Intent(context, WiFiReceiver.class);
        wifiIntent.putExtra(WifiManager.EXTRA_NETWORK_INFO, networkInfo);
        context.sendBroadcast(wifiIntent);

        // 2. Caso não exista conectividade por Wi-Fi o receiver não faz nada e é enviado um segundo
        // broadcast via intent explícito para MobileReceiver.class, por forma a averiguar se
        // existe conectividade mobile. Se existe e os serviços de localização estão activos, é
        // iniciada a localização por GPS, caso contrário o utilizador depara-se com um Dialog
        // a perguntar se deseja activar os serviços.
        context.sendBroadcast(new Intent(context, MobileReceiver.class));

        /*
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        boolean isConnected =  networkInfo != null &&
                networkInfo.isConnected();
        boolean isWiFi = isConnected &&
                networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
        boolean isMobile = isConnected &&
                networkInfo.getType() == ConnectivityManager.TYPE_MOBILE;

        if(isWiFi){
            Log.d(TAG, "Connected on Wi-Fi.");
            // Envia broadcast explícito para partilhar localização, o Wi-Fi pode já estar ligado.
            Toast.makeText(context, "Localização por Wi-Fi", Toast.LENGTH_SHORT).show();
            context.sendBroadcast(new Intent(context, WiFiReceiver.class));
        }else if(isMobile) {
            boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if(isGpsEnabled){
                Log.d(TAG, "GPS Enabled, requesting updates.");
               locationManager.requestLocationUpdates(
                       LocationManager.GPS_PROVIDER,
                       0,
                       0,
                       locationListener);
            }else{
                Log.d(TAG, "GPS not enabled, asking user to enable it");
                new AlertDialog.Builder(context)
                    .setMessage(R.string.location_services_needed)
                    .setCancelable(true)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            context.startActivityForResult(
                                    new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    })
                    .create()
                    .show();
            }
        }
        */
    }

    private static class GPSLocationListener implements LocationListener{

        private final Context context;

        public GPSLocationListener(Context context){
            this.context = context;
        }

        private Location lastLocation = null;

        public void onLocationChanged(Location location) {
            // Caso exista última localização conhecida, compara a distância para compreender
            // se a localização actual tem interesse.
            if(lastLocation != null){
                float dist = location.distanceTo(lastLocation);
                if(dist < LocationNegotiator.LOCATION_MINIMUM_DISTANCE){
                    Log.d(TAG, "Last location is near current location, ignore.");
                    // Se a última localização conhecida aconteceu a menos de 50m da mais
                    // recente, não tem interesse.
                    return;
                }
            }
            lastLocation = location;
            Log.d(TAG, String.format("%f:%f",
                    location.getLatitude(),
                    location.getLongitude()));
            new ParsePost(location).saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e != null){
                        // Notifica o adapter acerca de alterações nos dados
                        /*
                        Log.d(TAG, "Notifying adapter about changes");

                        LocationNegotiator
                                .getInstance(context)
                                .getAdapter()
                                .notifyDataSetChanged();
                                */
                    }
                }
            });
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {}

        public void onProviderEnabled(String provider) {}

        public void onProviderDisabled(String provider) {}

    }

}
