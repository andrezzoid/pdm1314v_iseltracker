package pt.isel.pdm.iseltracker.app.receivers;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.parse.ParseException;
import com.parse.SaveCallback;

import pt.isel.pdm.iseltracker.app.LocationNegotiator;
import pt.isel.pdm.iseltracker.app.R;
import pt.isel.pdm.iseltracker.app.models.ParsePost;
import pt.isel.pdm.iseltracker.app.utils.NetworkUtils;

/**
 * Created by André Jonas on 01/06/2014.
 */
public class MobileReceiver extends BroadcastReceiver {
    public static final String TAG = MobileReceiver.class.getSimpleName();


    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "onReceive(" + action + ")");

        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isConnected = mobile != null && mobile.isConnected();

        LocationManager locationManager =
                (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = LocationNegotiator
                .getInstance(context)
                .getLocationListener();

        if(isConnected){
            Log.d(TAG, "Mobile network is ON.");
            boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if(isGpsEnabled){
                Log.d(TAG, "GPS Enabled, requesting updates.");
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        0,
                        0,
                        locationListener);
            }else{
                Log.d(TAG, "GPS disabled, asking user to enable it.");
                new AlertDialog.Builder(context)
                    .setMessage(R.string.location_services_needed)
                    .setCancelable(true)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            context.startActivity(
                                    new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing
                        }
                    })
                    .create()
                    .show();
            }
        }else{
            locationManager.removeUpdates(locationListener);
        }
    }
}
