package pt.isel.pdm.iseltracker.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import pt.isel.pdm.iseltracker.app.LocationNegotiator;
import pt.isel.pdm.iseltracker.app.models.ParsePost;
import pt.isel.pdm.iseltracker.app.utils.NetworkUtils;

/**
 * Created by André Jonas on 30/05/2014.
 */
public class WiFiReceiver extends BroadcastReceiver {
    private static final String TAG = WiFiReceiver.class.getSimpleName();

    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "onReceive(" + action + ")");

        // Broadcast intent action indicating that the state of Wi-Fi connectivity has changed.
        // http://developer.android.com/reference/android/net/wifi/WifiManager.html
        // http://stackoverflow.com/questions/14839576/constantly-check-for-wireless-network-change

        // É mecessário requerer o ConnectivityManager, o WiFiManager e verificar o estado da ligação
        // pois este Receiver pode responder a outros broadcasts, nomeadamente o
        // pt.isel.pdm.iseltracker.receivers.FORCE_ACQUISITION, que é enviado pelo utilizador quando
        // permite "in-app" a partilha da localização.

        /*
        if(NetworkUtils.isConnectedWiFi(context)){
            Log.d(TAG, "Connected on Wi-Fi");
            WifiManager myWifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = myWifiManager.getConnectionInfo();
            String bssid = wifiInfo.getBSSID();
            Log.d(TAG, "BSSID: " + bssid);

            new ParsePost(bssid).saveInBackground();
        }
        */

        NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        boolean isConnected = info != null && info.isConnected();
        if(isConnected && info.getType() == ConnectivityManager.TYPE_WIFI){
            WifiManager myWifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = myWifiManager.getConnectionInfo();
            String bssid = wifiInfo.getBSSID();
            Log.d(TAG, "BSSID: " + bssid);

            new ParsePost(bssid).saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e != null){
                        // Notifica o adapter acerca de alterações nos dados
                        Log.d(TAG, "Notifying adapter about changes");
                    }
                }
            });
        }
    }
}
