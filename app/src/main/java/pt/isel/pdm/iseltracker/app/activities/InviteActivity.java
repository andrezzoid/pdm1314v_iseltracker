package pt.isel.pdm.iseltracker.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import pt.isel.pdm.iseltracker.app.R;
import pt.isel.pdm.iseltracker.app.adapters.ParseUserInviteAdapter;
import pt.isel.pdm.iseltracker.app.models.ParseFollowers;

public class InviteActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);

        ParseUserInviteAdapter adapter = new ParseUserInviteAdapter(
                this,
                new ParseQueryAdapter.QueryFactory<ParseUser>() {
                    @Override
                    public ParseQuery<ParseUser> create() {
                        // Todos os utilizadores que o utilizador corrente segue
                        ParseQuery<ParseFollowers> followMe = new ParseQuery<ParseFollowers>(ParseFollowers.class);
                        followMe.whereEqualTo(ParseFollowers.FOLLOWER, ParseUser.getCurrentUser());
                        // Subtrai à lista de utilizadores o resultado da query anterior
                        ParseQuery<ParseUser> users = new ParseQuery<ParseUser>(ParseUser.class);
                        users.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
                        users.whereDoesNotMatchKeyInQuery("objectId", ParseFollowers.FOLLOWING, followMe);
                        return users;
                    }
                }
        );

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_done) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
