package pt.isel.pdm.iseltracker.app;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;

import pt.isel.pdm.iseltracker.app.models.ParseFollowers;
import pt.isel.pdm.iseltracker.app.models.ParseLabel;
import pt.isel.pdm.iseltracker.app.models.ParsePost;

/**
 * Created by André Jonas on 20/05/2014.
 */
public class ISELTrackerApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // Logging
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        // Register Subclasses
        ParseObject.registerSubclass(ParsePost.class);
        ParseObject.registerSubclass(ParseLabel.class);
        ParseObject.registerSubclass(ParseFollowers.class);
        // Initialize with AppID and Client Key
        Parse.initialize(this, "Kq5ZolxgIoCkKxfNRpuz52vR7Ak5Q0zWn66fYEIc", "jJ4qbPJcGU93aMGCDB6EIEvgQIlmxtEkLU0aEWkU");
    }
}
