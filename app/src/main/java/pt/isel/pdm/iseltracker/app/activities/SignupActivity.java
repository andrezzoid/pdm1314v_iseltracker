package pt.isel.pdm.iseltracker.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import pt.isel.pdm.iseltracker.app.R;


public class SignupActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        final EditText studentNrEdit = (EditText) findViewById(R.id.studentNr);
        final EditText pwdEdit = (EditText) findViewById(R.id.password);
        final EditText confirmPwdEdit = (EditText) findViewById(R.id.confirmPassword);

        Button register = (Button) findViewById(R.id.register);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder errorMessage = new StringBuilder();
                boolean error = false;

                String studentNr = studentNrEdit.getText().toString();
                String pwd = pwdEdit.getText().toString();
                String confirmPwd = confirmPwdEdit.getText().toString();

                if(isEmpty(studentNr) || !isInteger(studentNr)){
                    error = true;
                    errorMessage.append(getString(R.string.invalidNumber));
                    errorMessage.append("\n");
                }

                if(isEmpty(pwd)){
                    error = true;
                    errorMessage.append(getString(R.string.invalidPassword));
                    errorMessage.append("\n");
                }

                if(!isMatching(pwd, confirmPwd)){
                    error = true;
                    errorMessage.append(getString(R.string.passwordsMismatch));
                    errorMessage.append("\n");
                }

                if(error){
                    Toast.makeText(SignupActivity.this, errorMessage.toString(), Toast.LENGTH_SHORT)
                            .show();
                }else{
                    // Set up a new Parse user
                    ParseUser user = new ParseUser();
                    user.setUsername(studentNr);
                    user.setPassword(pwd);
                    // Call the sign up method
                    user.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            // Handle the response
                            if (e != null) {
                                // Show the error message
                                Toast.makeText(SignupActivity.this, e.getMessage(),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                // Start an intent for the dispatch activity
                                Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }
                    });
                }
            }
        });

        TextView signin = (TextView) findViewById(R.id.signin);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, SigninActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isEmpty(String s){
        return s.trim().length() == 0;
    }

    private boolean isMatching(EditText etText1, EditText etText2) {
        if (etText1.getText().toString().equals(etText2.getText().toString())) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isMatching(String pwd, String confirm){
        return pwd.equals(confirm);
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

}
