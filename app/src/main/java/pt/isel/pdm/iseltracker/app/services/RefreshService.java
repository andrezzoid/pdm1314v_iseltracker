package pt.isel.pdm.iseltracker.app.services;

import android.app.Service;
import android.content.Intent;
import android.content.Context;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

import pt.isel.pdm.iseltracker.app.activities.MainActivity;
import pt.isel.pdm.iseltracker.app.adapters.ParsePostListAdapter;
import pt.isel.pdm.iseltracker.app.models.ParseFollowers;
import pt.isel.pdm.iseltracker.app.models.ParsePost;

public class RefreshService extends Service {

    private static final String TAG = RefreshService.class.getSimpleName();

    public RefreshService() {
        super(/*"RefreshService"*/);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        (new AsyncTask<Void, Void, List<ParsePost>>() {

            @Override
            protected List<ParsePost> doInBackground(Void... params) {
                Log.d(TAG, "Refreshing...");
                Context ctx = RefreshService.this.getApplicationContext();
                // Utilizadores que o utilizador corrente segue
                ParseQuery<ParseFollowers> following = new ParseQuery<ParseFollowers>(ParseFollowers.class);
                following.whereEqualTo(ParseFollowers.FOLLOWER, ParseUser.getCurrentUser());
                // Cruzar com os utilizadores que publicaram conteúdo
                ParseQuery<ParsePost> query = new ParseQuery<ParsePost>(ParsePost.class);
                query.addDescendingOrder("createdAt");
                query.whereMatchesKeyInQuery(ParsePost.USER, ParseFollowers.FOLLOWING, following);
                query.setLimit(50);
                try {
                    List<ParsePost> posts = query.find();
                    return posts;
                } catch (ParseException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<ParsePost> posts) {
                if(MainActivity.listView != null){
                    MainActivity.listView.setAdapter(
                            new ParsePostListAdapter(
                                    getApplicationContext(),
                                    posts));
                }
            }
        }).execute();
        return Service.START_NOT_STICKY;
    }
}
